<?php
$router->define([
    '' => 'controllers/home.controller.php',
    'new-courses' => 'controllers/new-courses.controller.php',
    'owned-courses' => 'controllers/owned-courses.controller.php',
    'contact' => 'controllers/contact.controller.php',
    'register' => 'controllers/register.controller.php',
    'login' => 'controllers/login.controller.php',
    'logout' => 'controllers/logout.controller.php',
    'edit-account' => 'controllers/edit-account.controller.php',
    'cart' => 'controllers/cart.controller.php',
    'course-product' => 'controllers/course-product.controller.php',
    'search-results' => 'controllers/search-course.controller.php',
    'reset-password' => 'controllers/forgot-password.controller.php',
    'start-course' => 'controllers/start-course.controller.php',
    'forum-questions' => 'controllers/forum/forum.questions.controller.php',
    'admin' => 'controllers/admin/admin.controller.php',
    'users' => 'controllers/admin/admin.users.controller.php',
    'checkout'=> 'controllers/checkout.controller.php',
    'thank-you'=> 'controllers/thank-you.controller.php',
    'forum' => 'controllers/forum/forum.controller.php',
    'details' => 'controllers/forum/forum.details.controller.php'
]);
