<!DOCTYPE html>
<html lang="en" title="WFFLIX - Login">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/wfflix/styling/main.css" rel="stylesheet">
    <link href="/wfflix/styling/login.css" rel="stylesheet">
</head>
<body>

<div class="container-md body-background navbar-space">
    <div class="row justify-content-md-center">
        <div class="col-md-auto d-flex">
            <img src="/wfflix/images/Logo/LogoDark.png">
        </div>
    </div>
    <form class="form-login row justify-content-md-center" method="post">
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <div id="input_container">
                    <img src="/wfflix/images/icons/Sample_User_Icon.png" id="input_img">
                    <input type="text" name="emailInput" placeholder="Email" id="input" aria-label="<?= $status ?>" <?= $disabled ?>>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <div id="input_container">
                    <img src="/wfflix/images/icons/passwordIcon.png" id="input_img">
                    <input type="password" name="passwordInput" placeholder="Password" id="input" aria-label="<?= $status ?>" <?= $disabled ?>>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <input id="loginBtnStyle" type="submit" name="loginBtn" value="LOGIN" aria-label="<?= $status ?>" <?= $disabled ?>>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <div class="input_container-span"><span class="error_message"><p><?= $mail_err ?></p></span></div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <div class="input_container-span"><span class="error_message"><p><?= $login_err ?></p></span></div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <div id="input_container-a">
                    <a href="/wfflix/reset-password">Forgot password?</a>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <div id="input_container-a">
                    <a href="/wfflix/register">Not a member?</a>
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>