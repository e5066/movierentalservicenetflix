<!DOCTYPE html>
<html lang="en" title="WFFLIX - Register">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/wfflix/styling/main.css" rel="stylesheet">
    <link href="/wfflix/styling/login.css" rel="stylesheet">
    <link href="/wfflix/styling/register.css" rel="stylesheet">
</head>
<body>

<div class="container-md body-background navbar-space">
    <div class="row justify-content-md-center">
        <div class="col-md-auto d-flex">
            <img src="/wfflix/images/Logo/LogoDark.png">
        </div>
    </div>
    <form class="form-login row justify-content-md-center" method="post">
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <div id="input_container">
                    <img src="/wfflix/images/icons/email.png" id="input_img">
                    <input type="email" name="emailInput" placeholder="Email" id="input">
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <h3 style="color: <?= $color ?>"><?= $mail_msg ?></h3>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <div id="input_container">
                    <form method="post">
                        <input id="registerBtnStyle" type="submit" name="resetBtn" value="Reset password!">
                    </form>
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>
