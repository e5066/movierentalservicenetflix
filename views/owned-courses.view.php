<!DOCTYPE html>
<html lang="en" title="WFFLIX - Home">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/wfflix/styling/owned-courses.css" rel="stylesheet">

</head>
<body>
<div class="container navbar-space">
    <div class="row text-center py-4">
        <?php if ($boughtCourses != null) : for ($i = 0; $i < count($boughtCourses); $i++) : ?>
        <div class="col-md-4">
                <div class="card shadow">
                <!--IMAGE OF THE COURSES-->
                    <div>
                        <?php echo '<img src="data:image;base64,'.base64_encode($boughtCourses[$i][5]).'" class="img-fluid card-img-top" alt="Image" style="width: 350px; height: 195px;">'?>
                    </div>
                    <div class="card-body">
                <!--TITEL OF THE COURSES-->
                        <h5 class="card-title"> <?=$boughtCourses[$i][1]?></h5>
                <!--DESCRIPTION OF THE COURSES-->
                        <p class="card-text">
                            <?= $boughtCourses[$i][2] ?>
                        </p>
                <!--AUTHOR OF THE COURSES-->
                            <div class="author"> <?=$boughtCourses[$i][4]?></div>
                        <h5>
                <!--THE AMOUNT OF CHAPTERS-->
                            <small><class="text-secondary"> Chapters </small>
                            <span class="info text"><?=$boughtCourses[$i][3]?></span>
                        </h5>
                        <a href="/wfflix/start-course?id=<?=$boughtCourses[$i][0]?>" class="btn btn-outline-success mt-5 float-end">Start course</a>
                    </div>
                </div>
        </div>
        <?php endfor; else: ?>
        <div class="col-lg-4 col-lg-offset-4"></div>
            <div class="card" style="width: 21rem; height: 15rem">
                <div class="card-body">
                    <h5 class="card-title" style="font-family: 'Montserrat', sans-serif">No courses here!</h5>
                    <p class="card-text">Sorry, you don't have any courses yet, go to the shop to buy one!</p>
                    <a href="/wfflix/new-courses" class="btn btn-primary" style="color: black">SHOP</a>
                </div>
            </div>
            <?php endif; ?>
    </div>
</div>
</body>
</html>







