<!DOCTYPE html>
<html lang="en" title="WFFLIX - Shop">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/wfflix/styling/buy-courses.css" rel="stylesheet">
    <link href="/wfflix/styling/main.css" rel="stylesheet">
</head>
<body>
<div class="container navbar-space">
    <div class="row ms-5 mt-5">
        <div class="col">
            <h1>Top picks</h1>
        </div>
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 col-xxl-8">
            <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                <?php if (!empty($top_picks)) : for ($i = 0; $i < count($top_picks); $i++) : ?>
                <div class="carousel-indicators">
                    <?php if ($i < 1) : ?>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="<?=$i?>" class="active" aria-current="true" aria-label="Slide <?=$i?>"></button>
                    <?php endif; ?>
                    <?php  for ($i = 0; $i < count($top_picks); $i++) : ?>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="<?=$i?>" aria-label="Slide <?=$i?>"></button>
                    <?php endfor;?>
                </div>
                <?php endfor; endif; ?>
                <div class="carousel-inner">
                    <?php if (!empty($top_picks)) : for ($i = 0; $i < count($top_picks); $i++) : ?>
                        <?php if ($i < 1) : ?>
                            <div class="carousel-item active">
                        <?php endif; ?>
                                <?php if ($i >= 1) : ?>
                                    <div class="carousel-item">
                                <?php endif; ?>
                                        <a href="/wfflix/course-product?id=<?=$top_picks[$i][7]?>">
                                            <?php echo '<img src="data:image;base64,'.base64_encode($top_picks[$i][4]).'" class="img-fluid" alt="Image" style="width: 100%; height: auto; margin-top: 2%;">'?>
                                        </a>
                                        <div class="carousel-caption d-none d-md-block carousel-info">
                                            <h5><?=$top_picks[$i][0]?></h5>
                                            <p class="card-text">
                                                <?php switch($top_picks[$i][5]):
                                                    case 1:
                                                        echo "<img src='/wfflix/images/scores/score-1.png' width='100px' height='25px'>";
                                                        break;
                                                    case 2:
                                                        echo "<img src='/wfflix/images/scores/score-2.png' width='100px' height='25px'>";
                                                        break;
                                                    case 3:
                                                        echo "<img src='/wfflix/images/scores/score-3.png' width='100px' height='25px'>";
                                                        break;
                                                    case 4:
                                                        echo "<img src='/wfflix/images/scores/score-4.png' width='100px' height='25px'>";
                                                        break;
                                                    case 5:
                                                        echo "<img src='/wfflix/images/scores/score-5.png' width='100px' height='25px'>";
                                                        break;
                                                endswitch;
                                                ?>
                                            </p>
                                            <p class="card-text">€<?=$top_picks[$i][3]?><small id="old-price"> €<?=$top_picks[$i][3] + 25?></small></p>
                                            <p class="card-text" style="margin-top: -20px;"><small>Author: <?=$top_picks[$i][8]?></small></p>
                                        </div>
                                    </div>
                    <?php endfor; endif; ?>
                    </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"><i class="bi bi-arrow-left-circle" style="font-size: 3vw"></i></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"><i class="bi bi-arrow-right-circle" style="font-size: 3vw"></i></span>
                    <span class="visually-hidden">Next</span>
                </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="margin-top: 5% !important;">
    <hr class="divider-solid">
    <div class="row justify-content-start">
        <div class="col-12 col-sm-12 col-md-2 col-lg-2 align-self-start col-lg-offset-3">
            <form action="/wfflix/new-courses" method="post" id="filterSystem">
                <div class="dropdown">
                    <?php if (isset($_POST['submitFilter']) || isset($_POST['orderBy'])) : ?>
                    <h5>results: <?= count($foundSubjects) ?></h5>
                    <?php endif; ?>
                    <select name="orderBy" onchange="onSelectChange();" class="form-select btn btn-secondary dropdown-toggle w-75 p-3">
                        <option selected>Order by</option>
                        <option value="most-popular">Most popular</option>
                        <option value="highly-rated">Highly rated</option>
                        <option value="newest">Newest</option>
                    </select>
                </div>
                <hr class="divider-solid">
                <h3>Scores</h3>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="scores" id="flexRadioDefault1" value="5">
                    <label class="form-check-label" for="flexRadioDefault1">
                        <img src="/wfflix/images/scores/score-5.png" height="30px" width="125px" style="margin-top: -5px !important;">
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="scores" id="flexRadioDefault2" value="4">
                    <label class="form-check-label" for="flexRadioDefault2">
                        <img src="/wfflix/images/scores/score-4.png" height="30px" width="125px" style="margin-top: -5px !important;">
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="scores" id="flexRadioDefault3" value="3">
                    <label class="form-check-label" for="flexRadioDefault3">
                        <img src="/wfflix/images/scores/score-3.png" height="30px" width="125px" style="margin-top: -5px !important;">
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="scores" id="flexRadioDefault4" value="2">
                    <label class="form-check-label" for="flexRadioDefault4">
                        <img src="/wfflix/images/scores/score-2.png" height="30px" width="125px" style="margin-top: -5px !important;">
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="scores" id="flexRadioDefault5" value="1">
                    <label class="form-check-label" for="flexRadioDefault5">
                        <img src="/wfflix/images/scores/score-1.png" height="30px" width="125px" style="margin-top: -5px !important;">
                    </label>
                </div>
                <hr class="divider-solid">
                <h3>Subject</h3>
                <select name="filterSubject" class="form-select" aria-label="Default select example">
                    <option selected>Select subject</option>
                    <?php ?>
                        <?php for ($i = 0; $i < count($subjects); $i++) : ?>
                            <option value="<?=$subjects[$i][0]?>"><?=$subjects[$i][0]?></option>
                        <?php endfor; ?>
                    <?php ?>
                </select>
                <hr class="divider-solid">
                <h3>Level</h3>
                <div class="form-check" style="margin-bottom: 0px !important;">
                    <input class="form-check-input" type="radio" name="levels" value="1" id="flexRadioDefault6">
                    <label class="form-check-label" for="flexRadioDefault6">
                        Beginner
                    </label>
                </div>
                <div class="form-check" style="margin-bottom: 0px !important;">
                    <input class="form-check-input" type="radio" name="levels" value="2" id="flexRadioDefault7">
                    <label class="form-check-label" for="flexRadioDefault7">
                        Intermediate
                    </label>
                </div>
                <div class="form-check" style="margin-bottom: 0px !important;">
                    <input class="form-check-input" type="radio" name="levels" value="3" id="flexRadioDefault8">
                    <label class="form-check-label" for="flexRadioDefault8">
                        Advanced
                    </label>
                </div>
                <div class="form-check" style="margin-bottom: 0px !important;">
                    <input class="form-check-input" type="radio" name="levels" value="4" id="flexRadioDefault9">
                    <label class="form-check-label" for="flexRadioDefault9">
                        Expert
                    </label>
                </div>
                <div class="submitFilters">
                    <input name="submitFilter" type="submit" value="Find" id="filterButton">
                </div>
            </form>
        </div>
        <div class="col-12 col-sm-12 col-md-10 col-lg-10">
            <div class="row justify-content-center">
            <?php if (isset($_POST['submitFilter']) || isset($_POST['orderBy']) || count($foundSubjects) >= 1) : ?>
            <?php for ($i = 0; $i < count($foundSubjects); $i++) : ?>
                    <div class="card mt-5" style="padding-top: 25px !important; border: transparent; border-top: 2px solid #6e6e6e; border-radius: 0px !important;">
                        <div class="row">
                            <div class="col-md-3 course-image">
                                <?php echo '<img src="data:image;base64,'.base64_encode($foundSubjects[$i][4]).'" class="img-fluid alt="Image" style="width: 350px; height: 195px;">'?>
                            </div>
                            <div class="col-md-6">

                                <h2 class="card-title mt-2"><?=$foundSubjects[$i][0]?></h2>
                                <h6 class="card-text">Author: <?=$foundSubjects[$i][8]?></h6>
                                <p class="card-text">
                                    <?php switch($foundSubjects[$i][5]):
                                        case 1:
                                            echo "<img src='/wfflix/images/scores/score-1.png' width='100px' height='25px'>";
                                            break;
                                        case 2:
                                            echo "<img src='/wfflix/images/scores/score-2.png' width='100px' height='25px'>";
                                            break;
                                        case 3:
                                            echo "<img src='/wfflix/images/scores/score-3.png' width='100px' height='25px'>";
                                            break;
                                        case 4:
                                            echo "<img src='/wfflix/images/scores/score-4.png' width='100px' height='25px'>";
                                            break;
                                        case 5:
                                            echo "<img src='/wfflix/images/scores/score-5.png' width='100px' height='25px'>";
                                            break;
                                    endswitch;
                                    ?>
                                </p>
                                <h6 class="card-text"><?=$foundSubjects[$i][1]?></h6>
                                <h6 class="card-text">Rating: <?=$foundSubjects[$i][5]?></h6>
                                <h6 class="card-text">Total chapters: <?=$foundSubjects[$i][2]?></h6>
                            </div>
                            <div class="col-md-3">
                                <h6 class="card-subtitle mb-2 text-muted mt-2 float-end">€<?=$foundSubjects[$i][3]?></h6>
                                <a href="/wfflix/course-product?id=<?=$foundSubjects[$i][7]?>" class="btn btn-outline-success mt-5 float-end">View course</a>
                            </div>
                        </div>
                    </div>
            <?php endfor; ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<script>function onSelectChange(){
        document.getElementById('filterSystem').submit();
    }
</script>
</body>
</html>