
<!doctype html>
<html lang="en">
  <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="/wfflix/styling/main.css">
      <link href="/wfflix/styling/contact.css" rel="stylesheet">
  </head>
  <body>



      <div class="container mt-5 navbar-space">
          <div class="row d-flex justify-content-center">
          <form class="contact-for col-12 col-sm-12 col-md-6 col-lg-6 justify-content-center text-center" method="post">
              <div class="form-row">
                  <div class="form-group">
                      <h2 class="text-center">Have some questions?</h2>
                      <label for="inputFirstName"></label>
                      <input type="text" name="firstName" class="form-control" id="inputFirstName" placeholder="First name">
                  </div>
                  <div class="form-group text-center">
                      <label for="inputLastName"></label>
                      <input type="text" name="lastName" class="form-control" id="inputLastName" placeholder="Last name">
                  </div>
              </div>
              <div class="form-group">
                  <label for="inputAddress"></label>
                  <input type="text" name="mail" class="form-control" id="inputEmail" placeholder="What's your email?">
              </div>
              <div class="form-group">
                  <label for="inputAddress"></label>
                  <input type="text" name="subject" class="form-control" id="inputSubject" placeholder="Subject">
              </div>
              <div class="form-group">
                  <label for="exampleFormControlTextarea1"></label>
                  <textarea class="form-control" name="message" id="exampleFormControlTextarea1" placeholder="Your questions..." rows="3"></textarea>
                  <input type="submit" name="submit" class="btn-send btn-dark mt-4" id="btn-message" value="Send Message">
              </div>
          </form>
          <?php if($success != "") : ?>
              <h2 class="text-center mt-3"><?= $success ?></h2>
          <?php else : ?>
              <h2 class="text-center mt-3"><?= $failed ?></h2>
          <?php endif; ?>
          </div>
      </div>
  </body>
</html>