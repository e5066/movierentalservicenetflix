<?php
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/wfflix/styling/checkout.css" rel="stylesheet">
</head>
<body class="bg-light">






<div class="container navbar-space">
    <div class="row d-flex justify-content-center">
        <div class="col col-12 col-sm-12 col-md-6 col-lg-6 justify-content-center text-center">

            <h1 class="head1 mt-5 text-center"><?="The following product(s) will be added to your account: " ?></h1>
        </div>
        <div class="col-10 ">
            <p class="lead mt-5" id="bold1"></p>
            <p class="lead mt-5" ></p>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Product</th>
                    <th scope="col">Price</th>
                </tr>
                </thead>

                <?php if (!empty($_SESSION['cart_items'])) : ?>
                    <?php for ($i = 0; $i < count($_SESSION['cart_items']); $i++) : ?>
                        <tbody id="bold2">
                        <tr>
                            <?php for ($y = 0; $y < count($_SESSION['cart_items'][$keys[$i]]) - 1; $y++) : ?>
                                <td><?= $_SESSION['cart_items'][$keys[$i]][$y]; ?></td>
                            <?php endfor; ?>

                        </tr>
                        </tbody>
                    <?php endfor; ?>
                <?php endif; ?>
            </table>
        </div>

            <div class="thank_you d-flex col-12 justify-content-center mb-5 pb-5">
                <form method="post">
                    <a href="/wfflix/thank-you"><input class="btn-home btn-dark mt-5 ps-5 pe-5 backShopBtn text-center"  name="confirm" id="confirm1"  type="submit" value="Confirm Order"></a>
                </form>
            </div>
    </div>
</div>
</body>
</html>
