<head>
    <link href="/wfflix/styling/cart.css" rel="stylesheet">
</head>
<body>
<div class="container ms-auto me-auto navbar-space mt-5">
    <div class="row d-flex justify-content-center pt-5">
        <p class="lead mt-1 mb-5 ms-5" id="bold1">Shopping Cart</p>
        <div class="col-12 ms-5" style="overflow-y: scroll; tab-index: 0; height: 15rem;">
            <div data-bs-spy="scroll" data-bs-target="#navbar-example2" data-bs-offset="0" class="scrollspy-example" tabindex="0">



                <table class="table">

                    <tr>
                        <th scope="col">Product</th>
                        <th scope="col">Price</th>
                    </tr>

                <?php if (!empty($_SESSION['cart_items'])) : ?>
                    <?php for ($i = 0; $i < count($_SESSION['cart_items']); $i++) : ?>
                    <tbody>
                        <tr>
                        <?php for ($y = 0; $y < count($_SESSION['cart_items'][$keys[$i]]) - 1; $y++) : ?>
                            <td><?= $_SESSION['cart_items'][$keys[$i]][$y]; ?></td>
                        <?php endfor; ?>
                            <td>
                                <form method="post">
                                <input name="<?= $_SESSION['cart_items'][$keys[$i]][2] ?>" type="submit" id="bold2" class="btn" value="Delete">
                                </form>
                            </td>
                        </tr>
                    </tbody>
                    <?php endfor; ?>
                <?php endif; ?>
            </table>

            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 mb-5 ">
            <form class="contact-for" method="post">
                <div class="form-group">
                    <input class="btn-checkout btn-dark text-center" id="btn-checkout" name="goToCheckout" type="submit" value="Checkout">
                </div>
            </form>
        </div>
    </div>
</div>
</body>