<!DOCTYPE html>
<html lang="en" title="WFFLIX - Home" xmlns="http://www.w3.org/1999/html">


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/wfflix/styling/homepage.css" rel="stylesheet">
    <link href="'https://fonts.googleapis.com/css2?family=Montserrat&display=swap'" rel="stylesheet">

    <footer>
        <div class="container text-center text-md-start mt-5">
            <div class="row d-flex justify-content-center">
                <div class="col-6 col-md-3 col-lg-2 mb-4">
                    <h6>
                        <a href="/wfflix/">
                            <img src="/wfflix/images/Logo/LogoDark.png" class="img-fluid">
                        </a>
                    </h6>
                </div>
                <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                    <h6 class="text-uppercase fw-bold mb-4">
                        Subjects
                    </h6>
                    <a href="/wfflix/new-courses">
                        <div>Web development</div>
                        <div>Developer tools</div>
                        <div>Machine learning</div>
                    </a>
                </div>
                <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                    <h6 class="text-uppercase fw-bold mb-4">
                        Our office
                    </h6>
                    <a href="https://goo.gl/maps/oqt6HzFCBZWBXZDWA">
                        <div>Hospitaaldreef 5</div>
                        <div>1315 RC Almere</div>
                        <div>Netherlands</div>
                    </a>
                </div>
                <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                    <img src="/wfflix/images/socials.png" height="150" width="300" alt="Workplace" usemap="#socialmedia">

                    <map name="socialmedia">
                        <area target="_top" alt="q" title="facebook" href="https://nl-nl.facebook.com/"
                              coords="32,75,32" shape="circle">
                        <area target="_top" alt="q" title="instagram" href="https://www.instagram.com/"
                              coords="112,76,30" shape="circle">
                        <area target="_top" alt="q" title="twitter" href="https://twitter.com/?lang=nl"
                              coords="191,75,31" shape="circle">
                        <area target="_top" alt="q" title="linkedin" href="https://nl.linkedin.com/"
                              coords="269,75,30" shape="circle">
                    </map>
                </div>
            </div>
        </div>
    </footer>
    </body>

</html>