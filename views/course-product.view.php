


<?php

?>


<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/wfflix/styling/course-product.css" rel="stylesheet">

</head>
<body>

<div class="container mt-5 navbar-space">
    <div class="row">
        <div class="col-12">
            <p class="lead" id="bold1"><?=$collectedCourses1[0][1]?></p>  <!--title-->
            <p class="lead" id="bold2">€<?=$collectedCourses1[0][5]?></p> <!--price-->
            <p class="lead" id="description"><?=$collectedCourses1[0][2]?></p>  <!--description-->
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-8 mt-1">
            <?php ?>
            <?php echo '<img src="data:image;base64,'.base64_encode($collectedCourses1[0][6]).'" class="img-fluid alt="Image" style="width: 450px; height: 195px;">'?>
            <?php ?>
            <p class="lead mt-3" id="bold3">Chapters:   <?=$collectedCourses1[0][3]?></p>    <!--chapters-->
            <p class="lead" id="bold4">Author: </p>                                     <!--title-->
            <p class="lead" id="bold5">Level:   <?=$collectedCourses1[0][8]?></p>       <!--level-->
            <p class="lead" id="bold6">date of issue: <?=$collectedCourses1[0][9]?></p> <!--date-->
            <form method="post">
                <?php if ($hasCourse == true) : ?>
                    <input class="btn btn-dark mt-4" onclick="window.location.href='/wfflix/owned-courses'" id="btn-buy" name="buyBtn" type="button" value="In library">
                <?php else : ?>
                    <input class="btn btn-dark mt-4" onclick="window.location.href='<?= ($setValue == "Buy" ? "/wfflix/login" : "/wfflix/cart") ?>'" id="btn-buy" name="buyBtn" type="button" value="<?= $setValue ?>">
                <?php endif; ?>
            </form>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-4 mt-5">
            <img class="img-help" src="/wfflix/images/help.png" alt="my help">
            <p class="lead mt-4" id="bold7">Questions about this course?</p>
            <p class="lead">We are happy to help you with personal study advice.</p>
            <form method="post">
                <a href="/wfflix/contact" class="btn-home btn-dark mt-4 ps-5 pe-5 goToContact text-center" id="btn-message">Contactpagina</a>
            </form>
        </div>
    </div>

</div>
</body>
</html>
