<div class="container-fluid d-flex background-image" style="height: 85vh">
    <div class="d-flex flex-grow-1 justify-content-center align-items-center">
        <div class="container-title">
            <div><h1>A versatile</h1></div>
            <div class='indent-b'><h1>range of courses</h1></div>
            <div class='indent-c'><p>Become a programmer in<br>no time</div>
        </div>
    </div>

</div>

<div class="container-fluid d-flex footer-black" style="height: 6vh">
    <div class="d-flex flex-grow-1 justify-content-left align-items-left">

        <p>Never forget where you started due<br>to our advanced systems </p>
    </div>
</div>

<div class="container-fluid mt-5" style="min-height: 40vh">
    <div class="row">
        <div class="col-md-6 col-sm">
            <div class="row mb-4">
                <div class="col-10">
                    <div class="card text-white text-center py-3 rectangle-1">

                        <h2><a href="/wfflix/register" style="text-decoration: none; color: white"> Join us today</a></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card text-white text-center py-3 rectangle-1">
                        <h2><a href="/wfflix/new-courses" style="text-decoration: none; color: white">Visit our courses</a></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <img src="/wfflix/images/trustpilot.png" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm d-flex align-items-center">
            <img src="/wfflix/images/courses.png" class="img-fluid"/>
        </div>
    </div>
</div>
