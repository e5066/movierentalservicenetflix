<!DOCTYPE html>
<html lang="en" title="WFFLIX - <?= $pageTitle ?>">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.1/font/bootstrap-icons.css">
    <link href="/wfflix/styling/main.css" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light sticky-top" style="background-color: white; -webkit-box-shadow: 0px 6px 11px 0px rgba(0,0,0,0.89);
box-shadow: 0px 6px 11px 0px rgba(0,0,0,0.89);">
        <div class="container-fluid">
            <a class="navbar-brand" href="/wfflix/">
                <img src="/wfflix/images/Logo/LogoDark.png" alt="" width="135" height="auto">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse text-center text-sm-center text-md-center" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-5 ms-xl-5">
                    <?php if (!empty($_SESSION['userName'])) : ?>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/wfflix/owned-courses">Owned courses</a>
                    </li>
                    <?php endif; ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/wfflix/new-courses">Shop</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/wfflix/forum">Forum</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/wfflix/contact">Contact</a>
                    </li>
                </ul>
                <form method="post" class="d-flex flex-fill ms-lg-5 ms-xl-5">
                    <input class="form-control me-2 search-bar" type="search" onchange="searchCourses(this.value)" placeholder="Search course" aria-label="Search">
                </form>
                <?php if (!empty($_SESSION['userName'])) : ?>
                <div class="row mt-4 mt-sm-4 mt-md-4 mt-lg-0 mt-xl-0">
                    <div class="col-12 d-flex justify-content-center">
                        <div class="dropdown ms-4 me-4">
                            <button class="btn-login dropdown-toggle ps-5 pe-5" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false" type="button"><?=$user_name?></button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="#">Orders</a></li>
                                <li><a class="dropdown-item" href="/wfflix/edit-account">Settings</a></li>
                                <li><a class="dropdown-item" href="/wfflix/logout">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php else :?>
                <button class="btn-login btn-login-normal ps-5 pe-5 ms-4 me-4" onclick="location.href='<?= ($user_name == "Sign in" ? "/wfflix/login" : "/wfflix/edit-account")?>'" type="button"><?=$user_name?></button>
                <?php endif; ?>
            </div>
        </div>
    </nav>
<script>
    function searchCourses(val){
        var queryParams = new URLSearchParams(window.location.search);
        queryParams.set("subject", val);
        history.replaceState(null, null, "?"+queryParams.toString());
    }

</script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>
</html>
