<?php
//include 'views/forum/forum.navigation.visitor.php';
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link href="/wfflix/styling/main.css" rel="stylesheet">
    <link href="/wfflix/styling/forum/forum.view.css" rel="stylesheet">
    <link href="/wfflix/styling/forum/forum.questions.css" rel="stylesheet">
</head>

<body>

<div class="container">
   <div>
       <button>add new card</button>
   </div>
<?php foreach ($amountofcards as $card){ ?>
    <div class="row">
        <div class="col-xl-1 col-lg-2 col-md-2 col-sm-4 col-4 card-data">
            <div class="row card-votes">
                <p>Votes: <?php echo $card[3] ?></p>
            </div>
            <div class="row card-answers">
                <p>Answers: <?php echo $card[4] ?></p>
            </div>
            <div class="row card-views">
                <p>Views: <?php echo $card[5] ?></p>
            </div>
        </div>
        <div class="col-xl-11 col-lg-10 col-md-10 col-sm-8 col-8 card-content">
            <div class="row">
                <div class="h4"><a href="/wfflix/details?var=<?php echo $card[0];?>"><?php echo $card[1];?></a></div>
            </div>

            <div class="row">
                <div class="description-details"><?php echo $card[2] ?></div>
            </div>
            <div class="row">
                <div class="row">
                    <div class="col-lg-8 col-md-6">
                        <button><?php echo $card[9] ?></button>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="description-small">created by <?php echo $card[8] ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
</div>


<!--<div class="wrapper-wrapper">-->

<!--</div>-->
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>
