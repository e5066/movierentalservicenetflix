<?php $detail_card_id = $_GET['var'];?><!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link href="/wfflix/styling/main.css" rel="stylesheet">
    <link href="/wfflix/styling/forum/forum.view.css" rel="stylesheet">
    <link href="/wfflix/styling/forum/forum.questions.css" rel="stylesheet">
</head>
<body>
<div class="container"><br>
    <?php foreach ($getHeader as $header){ ?>    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 card-content mx-auto rounded-top border-top">
            <div class="display-6">
                <div class=""><?php echo $header[0] ?></div>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 card-content mx-auto border-top">
                <br><div><?php echo $header[1] ?></div><br>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 card-content mx-auto rounded-bottom border-top">
                <div class="text-muted small-text"><p class="right">Created by: <?php echo $header[3]?></p></div>
            </div>
        </div><br>
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 card-content mx-auto border-top">
                <br>
                <div class="d-flex justify-content-center">
                    <form action="/models/Submit_answer.php?card=<?php echo $detail_card_id ?>" name="answerForm" method="post">
                        <textarea name="answerBox" cols="70" rows="4" required></textarea><br>
                        <button type="submit" class="btn btn-outline-secondary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    <?php } ?>    <?php foreach ($amountofcardsanswers as $card){ ?>        <div class="row ">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 card-content mx-auto border-bottom">
            <div class="row">
                <div><br><p><?php echo $card[1] ?></p></div>
            </div>
            <div class="row">
                <div class="text-muted small-text"><p class="right">Answered by: <?php echo $card[2] ?></p></div>
            </div>
        </div>
    </div>
    <?php } ?></div>
</body>

