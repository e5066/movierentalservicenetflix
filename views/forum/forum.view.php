<?php
include 'views/forum/forum.navigation.visitor.php';
?>

<!DOCTYPE html>
<html lang="en">
 <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link href="/wfflix/styling/main.css" rel="stylesheet">
    <link href="/wfflix/styling/forum/forum.view.css" rel="stylesheet">
 </head>

 <body>
  <div class="div-wrapper">
   <br><br>
   <h2 class="h2-titel-forum">We're here to help</h2>
   <!--Break-->
   <br><br>

      <div class="div-container-knowledge-support-questions">
          <div class="div-knowledge-base"> <!--Begin van Knowledge base-->
              <div><img src="" alt="Foto van Knowledge"></div> <!--Img-->
              <h2 class="h2-titel-knowledge-base">Knowledge base</h2>
              <p>KnowledgeKnowledgeKnowledgeKnowledgeKnow</p>
              <p>KnowledgeKnowledgeKnowledgeKnowledgeKnow</p>
              <p>KnowledgeKnowledgeKnowledgeKnowledgeKnow</p>
              <button class="btn-knowledge-base-learn-more"><a href="/wfflix/forum-questions" style="text-decoration: none; color: black">Learn more</a></button>
          </div> <!--Einde van Knowledge base-->

          <br><br>

          <div class="div-support"> <!--Begin van support-->
              <div><img src="" alt="Foto van Support"></div> <!--Img-->
              <h2 class="h2-titel-support">Support</h2>
              <p>SupportSupportSupportSupportSupportSupport</p>
              <p>SupportSupportSupportSupportSupportSupport</p>
              <p>SupportSupportSupportSupportSupportSupport</p>
              <button class="btn-support-solve">Solve</button>
          </div> <!--Einde van support-->

          <br><br>

          <div class="div-questions"> <!--Begin van questions-->
              <div><img src="" alt="Foto van Questions"></div> <!--Img-->
              <h2 class="h2-titel-questions">Questions</h2>
              <p>questionsquestionsquestionsquestionsquestions</p>
              <p>questionsquestionsquestionsquestionsquestions</p>
              <p>questionsquestionsquestionsquestionsquestions</p>

              <button class="btn-questions-answers-qa">Q&A</button>
          </div> <!--Einde van questions-->
          <br><br>
      </div> <!-- Einde van de container -->
  </div> <!-- Einde van de wrapper -->


<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>

<?php
?>




