<!DOCTYPE html>
<html lang="en" title="WFFLIX - Start course">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/wfflix/styling/main.css" rel="stylesheet">
    <link href="/wfflix/styling/start-course.css" rel="stylesheet">
</head>
        <div class="container-md container-fluid navbar-space">
            <div class="row mb-4">
                <div class="col-12 align-bottom">
                    <a href="/wfflix/owned-courses" title="Owned courses"><i class="bi bi-arrow-return-left" style="font-size: 28px"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 d-flex justify-content-center">
                    <iframe id="ytFrame" width="1080" height="480" src="<?= $course_url . $_GET['video'] ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="col-md-5 d-flex justify-content-center">
                    <div class="accordion" id="accordionPanelsStayOpenExample" style="width: 100% !important;">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne2" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                                    Course information:
                                </button>
                            </h2>
                            <div id="panelsStayOpen-collapseOne2" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                                <div class="accordion-body">
                                    <h3 class="align-text-bottom mt-auto"><?= $channelTitle ?></h3>
                                    <strong>Chapters: </strong><?= $counter ?><br>
                                    <strong>Rating: </strong><?= $rating ?> stars<br>
                                    <strong>Language: </strong><?= $language ?><br><br>
                                    <strong>Link: </strong><a href="https://www.youtube.com/channel/<?= $channel_url ?>" target="_blank"><?= $channelTitle ?></a><br>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne3" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                                    Next subject:
                                </button>
                            </h2>
                            <div id="panelsStayOpen-collapseOne3" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                                <div class="accordion-body">
                                    <?php if ($nextVideo != null) : ?>
                                    <a href="/wfflix/start-course?id=<?=$_GET['id']?>&video=<?= $nextVideo[2] ?>" style="color: black; text-decoration: none">
                                        <div class="card mb-3">
                                            <div class="row g-0">
                                                <div class="col-md-4" style="display: block !important; margin: auto">
                                                    <img src="<?= $nextVideo[3] ?>" class="img-fluid rounded-start" title="Start course: <?= $nextVideo[0] ?>">
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="card-body">
                                                        <h5 class="card-title"><?= $nextVideo[0] ?></h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <?php else: ?>
                                    <div class="card mb-3">
                                        <div class="row g-0">
                                            <div class="col-md-8">
                                                <div class="card-body">
                                                    <h5 class="card-title">Finished</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 d-flex pt-4">
                        <div class="accordion" id="accordionPanelsStayOpenExample1" style="width: 100% !important;">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                                        <?= $course_title ?>
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                                    <div class="accordion-body">
                                        <?= $course_description ?>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false" aria-controls="panelsStayOpen-collapseTwo">
                                        Subjects:
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingTwo">
                                    <div class="accordion-body">
                                        <?php if (!empty($course_items)) : for ($i = 0; $i < count($course_items); $i++) : ?>
                                        <a href="/wfflix/start-course?id=<?=$_GET['id']?>&video=<?= $course_items[$i][2] ?>" style="color: black; text-decoration: none">
                                            <div class="card mb-3">
                                                <div class="row g-0">
                                                    <div class="col-md-4" style="display: block !important; margin: auto">
                                                        <img src="<?= $course_items[$i][3] ?>" class="img-fluid rounded-start" title="Start course: <?= $course_items[$i][0] ?>">
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="card-body">
                                                            <h5 class="card-title"><?= $course_items[$i][0] ?></h5>
                                                            <p class="card-text"><?= wordwrap($course_items[$i][1], 425, "<br><br>") ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    <?php endfor; endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</body>
</html>