<?php
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/wfflix/styling/thank-you.css" rel="stylesheet">

</head>
<body class="bg-light">




<div class="container navbar-space">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center mb-4 mt-5"><?="Thank you for your purchase! "?></h1>
            <h5 class="text-center"><?=" The new course(s) can be found in your account. "?></h5>
        </div>
        <div class="thank_you d-flex col-12 justify-content-center">
            <a href="/wfflix/owned-courses"><input class="btn-home btn-dark mt-5 ps-5 pe-5 backShopBtn text-center"  name="goToOwnedCourses" id="toOwnedCourses" type="submit"  value="Go to Owned Courses"></a>
        </div>
        <div class="thank_you d-flex col-12 justify-content-center">
            <a href="/wfflix/new-courses"><input class="btn-home btn-dark mt-5 ps-5 pe-5 backHomeBtn text-center"  name="goToNewCourse"  id="toNewCourse" type="submit" value="Back to shop"></a>
        </div>
    </div>
</div>
</body>
</html>
