<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="/wfflix/styling/main.css" rel="stylesheet">
    <link href="/wfflix/styling/admin/admin.view.css" rel="stylesheet">
</head>
<body>

<div class="wrapper">
    <div class="side">
        <div class="nav-row row"><a href="/wfflix/forum"><span class="material-icons">add_circle_outline</span></a></div>
        <div class="nav-row row active"><a href=""><span class="material-icons">person</span></a></div>
        <div class="nav-row row"><a href="/wfflix/forum"><span class="material-icons">auto_stories</span></a></div>
        <div class="nav-row row"><a href="/wfflix/forum"><span class="material-icons">inventory_2</span></a></div>
    </div>
    <div class="content">
        <div class="container user-container">
            <div class="head-row row align-items-start">
                <div class="col"><b>Name</b></div>
                <div class="col"><b>Type</b></div>
                <div class="col"><b>ID</b></div>
                <div class="col"><b>School</b></div>
                <div class="col"><b>Date</b></div>
                <div class="col col-1"><b>Details</b></div>
                <div class="col col-1"><b>Block</b></div>
            </div>
            <?php foreach ($numbers as $number) { //echo var_dump($number)?>
                <div class="row user-row">
                    <div class="user col"><?php echo $number[2]," ",$number[3];?></div>
                    <div class="user col"><?php echo $number[2];?></div>
                    <div class="user col"><?php echo $number[6];?></div>
                    <div class="user col"><?php echo $number[7];?></div>
                    <div class="user col"><?php echo $number[8];?></div>
                    <div class="user col col-1 details" onclick="unfold('<?php echo $number[0];?>')">Details</div>
                    <div class="user col col-1 block">Block</div>
                </div>
                <div class="row user-row user-details hidden" id="<?php echo $number[0];?>">
                    <div class="user col detail-section">
                        <ul>
                            <li class="c-dark"><b>Name:</b>       <span class="span-right"><?php echo $number[2]," ",$number[3];?></span></li>
                            <li class="c-light"><b>Username:</b>  <span class="span-right"><?php echo $number[1]?></span></li>
                            <li class="c-dark"><b>Email:</b>      <span class="span-right"><?php echo $number[4]?></span></li>
                            <li class="c-light"><b>Type:</b>      <span class="span-right"><?php if($number[5] == 0) {echo "Student";} else if($number[5] == "1"){echo "Teacher";} else {echo "Admin";}?></span></li>
                            <li class="c-dark"><b>StudentID:</b>  <span class="span-right"><?php echo $number[6]?></span></li>
                            <li class="c-light"><b>Institute:</b> <span class="span-right"><?php echo $number[7]?></span></li>
                            <li class="c-dark"><b>Date:</b>       <span class="span-right"><?php echo $number[8]?></span></li>
                        </ul>
                    </div>
                    <div class="user col col-7"><!--Picture--></div>
                    <div class="user col col-1 details-close" onclick="fold('<?php echo $number[0];?>')"><b>Close</b></div>
                </div>
            <?php }  ?>
        </div>
    </div>
</div>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
<script>
    function unfold(user) {
        user = parseInt(user);
        let element = document.getElementById(user);
        element.classList.remove("hidden");
    }

    function fold(user) {
        user = parseInt(user);
        let element = document.getElementById(user);
        element.classList.add("hidden");
    }
</script>
</body>
</html>






