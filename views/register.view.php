<!DOCTYPE html>
<html lang="en" title="WFFLIX - Register">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/wfflix/styling/main.css" rel="stylesheet">
    <link href="/wfflix/styling/login.css" rel="stylesheet">
    <link href="/wfflix/styling/register.css" rel="stylesheet">
</head>
<body>

<div class="container-md body-background navbar-space">
    <div class="row justify-content-md-center">
        <div class="col-md-auto d-flex">
            <img src="/wfflix/images/Logo/LogoDark.png">
        </div>
    </div>
    <form class="form-login row justify-content-md-center" method="post">
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <div id="input_container">
                    <img src="/wfflix/images/icons/Sample_User_Icon.png" id="input_img">
                    <input type="text" name="nameInput" placeholder="Name" id="input">
                </div>
            </div>
            <div class="col-md-auto">
                <div id="input_container">
                    <img src="/wfflix/images/icons/Sample_User_Icon.png" id="input_img">
                    <input type="text" name="lastnameInput" placeholder="Lastname" id="input">
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <div id="input_container">
                    <img src="/wfflix/images/icons/email.png" id="input_img">
                    <input type="email" name="emailInput" placeholder="Email" id="input">
                </div>
            </div>
            <div class="col-md-auto">
                <div id="input_container">
                    <img src="/wfflix/images/icons/passwordIcon.png" id="input_img">
                    <input type="password" name="passwordInput" placeholder="Password" id="input">
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <div id="input_container">
                    <img src="/wfflix/images/icons/passwordIcon.png" id="input_img">
                    <input type="password" name="confirmPasswordInput" placeholder="Confirm password" id="input">
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <h4 style="color: <?= $color ?>; margin-top: 10px; text-align: center;"><?= $error_msg ?></h4>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto d-flex">
                <div id="input_container">
                    <input id="registerBtnStyle" type="submit" name="registerBtn" value="Become a member!">
                </div>
            </div>
        </div>
    </form>
</div>

</body>
</html>