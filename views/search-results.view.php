<?php


?>


<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container" style="margin-top: 50px !important;">
<?php ?>
<?php if (count($collectedCourses) === 0) : ?>
    <h2>Geen resultaten gevonden voor "<?=($_SESSION['subject'])?>"</h2>
<?php elseif (count($collectedCourses) === 1 ) : ?>
    <h2><?=count($collectedCourses)?> resultaat gevonden voor "<?=($_SESSION['subject'])?>"</h2>
<?php else : ?>
    <h2><?=count($collectedCourses)?> resultaten gevonden voor "<?=($_SESSION['subject'])?>"</h2>
<?php endif;?>
<?php ?>
</div>

<div class="container mt-5 navbar-space" style="margin-top: 50px !important;">
    <div class="row justify-content-center">
        <?php for ($i = 0; $i < count($collectedCourses); $i++) : ?>
                <div class="card mt-5" style="margin-top: 25px !important;">
                    <div class="row">
                        <div class="col-md-3">
                            <?php echo '<img src="data:image;base64,'.base64_encode($collectedCourses[$i][4]).'" class="img-fluid alt="Image" style="width: 350px; height: 195px;">'?>
                        </div>
                            <div class="col-md-6">

                                <h5 class="card-title mt-2"><?=$collectedCourses[$i][0]?></h5>
                                <p class="card-text"><?=$collectedCourses[$i][1]?></p>
                                </div>
                                    <div class="col-md-3">
                                        <h6 class="card-subtitle mb-2 text-muted mt-2 float-end">€<?=$collectedCourses[$i][3]?></h6>
                                        <a href="/wfflix/course-product?id=<?=$collectedCourses[$i][5]?>" class="btn btn-primary mt-5 float-end" id=$collectedCourses[$i][5]>View course</a>
                                    </div>
                    </div>
                </div>
        <?php endfor; ?>
    </div>
</div>
</body>
</html>