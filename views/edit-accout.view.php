<head>
    <link href="/wfflix/styling/edit-account.css" rel="stylesheet">
</head>
<body>

<div class="container navbar-space" style="font-family: 'Montserrat', sans-serif">
        <div class="col">
            <div class="row">
                <div class="col mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="e-profile">
                                <div class="row">
                                    <div class="col-12 col-sm-auto mb-3">
                                        <div class="mx-auto" style="width: 140vh;">
                                        </div>
                                    </div>
                                    <div class="col d-flex flex-column flex-sm-row justify-content-between mb-3">
                                        <div class="text-center text-sm-left mb-2 mb-sm-0">
                                            <h4 class="pt-sm-2 pb-1 mb-0 text-nowrap" style="font-weight: bold">Settings</h4>
                                        </div>
                                        <div class="text-center text-sm-right">
                                            <div class="text-muted"><small><p><span id="datetime"></span></p>
                                                    <script>
                                                        var dt = new Date();
                                                        document.getElementById("datetime").innerHTML = dt.toLocaleDateString();
                                                    </script></small></div>
                                        </div>
                                    </div>
                                </div>

                                            <form class="form" name="myform" novalidate="" method="post" onsubmit="return validateForm()" required>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label>Firstname</label>
                                                                    <input class="form-control" type="text" name="firstname" value=<?=$firstname?> placeholder=<?=$firstname?>>
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label>Lastname</label>
                                                                    <input class="form-control" type="text" name="lastname" value=<?=$lastname?> placeholder=<?=$lastname?>>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label>Username</label>
                                                                    <input class="form-control" type="text" name="username" value=<?=$username?> placeholder=<?=$username?>>
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label>Email</label>
                                                                    <input class="form-control" type="text" name="email" value=<?=$email?> placeholder=<?=$email?>>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col d-flex justify-content-end">
                                                        <div class="row justify-content-md-center">
                                                            <div class="col-md-auto d-flex">
                                                                <h4 style="color: red; margin-top: 10px; text-align: center;"><?= $edit_err ?></h4>
                                                            </div>
                                                        </div>
                                                    </div> <div class="row justify-content-md-center">
                                                        <div class="col-md-auto d-flex">
                                                            <h4 style=" margin-top: 10px; text-align: center;"><?= $succ_edit ?></h4>
                                                        </div>
                                                    </div>
                                                        <button class="btn btn-primary" style="background-color: #5C258D" type="submit" name="edit">Save Changes</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                        <form class="form" name="mypwform" novalidate="" method="post" action="#nav-pw">
                                            <div class="row">
                                                <div class="col-12 col-sm-6 mb-3">
                                                    <div class="mb-2"><b>Change Password</b></div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group">
                                                                <label>New Password</label>
                                                                <input class="form-control" type="password" placeholder="••••••" name="pw1">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group">
                                                                <label>Confirm <span class="d-none d-xl-inline">Password</span></label>
                                                                <input class="form-control" type="password" placeholder="••••••" name="pw2"></div>
                                                        </div>
                                                    </div>
                                                    <div class="row justify-content-md-center">
                                                        <div class="col-md-auto d-flex">
                                                            <h4 style="color: red; margin-top: 10px; text-align: center;"><?= $pw_err ?></h4>
                                                        </div>
                                                    </div>
                                                    <div class="row justify-content-md-center">
                                                        <div class="col-md-auto d-flex">
                                                            <h4 style="color: red; margin-top: 10px; text-align: center;"><?= $changed_pw_err ?></h4>
                                                        </div>
                                                    </div> <div class="row justify-content-md-center">
                                                        <div class="col-md-auto d-flex">
                                                            <h4 style=" margin-top: 10px; text-align: center;"><?= $success_msg ?></h4>
                                                        </div>
                                                    </div>
                                                    <button class="btn btn-primary" style="background-color: #5C258D" type="submit" name="changepw">change password</button>
                                                </div>
                                                </div>
                                        </form>

                                <div class="tab-content pt-3">
                                    <script>
                                        function validateForm() {
                                            var x = document.forms["myform"]["firstname"].value;
                                            var y = document.forms["myform"]["lastname"].value;
                                            var z = document.forms["myform"]["email"].value;
                                            if (x == "" || x == null) {
                                                alert("Name must be filled out");
                                                return false;
                                            }
                                            if (y == "" || y == null) {
                                                alert("Name must be filled out");
                                                return false;
                                            }
                                            if (z == "" || z == null) {
                                                alert("Email must be filled out");
                                                return false;
                                            }
                                        }
                                    </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title font-weight-bold">Support</h6>
                            <p class="card-text">Any questions? Let us know!</p>
                            <form action="contact">
                                <button type="submit" class="btn btn-primary" style="background-color: #5C258D">Contact Us</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

