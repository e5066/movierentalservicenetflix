<?php
session_start();
$pageTitle = "Checkout";
require_once 'models/Checkout_model.php';
$checkout_model = new Checkout_model();

global $keys;

$keys = [];

if (!empty($_SESSION['cart_items'])) {
    foreach
    ($_SESSION['cart_items'] as $key => $value) {
        array_push($keys, $key);
    }
}

if (!empty($_SESSION['userName'])){
    $user_name = $_SESSION['userName'];
}
else{
    $user_name = "Sign in";
}
if (isset($_GET['subject'])){
    $_SESSION['subject'] = $_GET['subject'];
    session_commit();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results");
}
$uid=$_SESSION['userId'];

//If customer confirms order with button.
if(isset($_POST['confirm'])) {
    $isDone = false;
    $order_date = date('Y-m-d H:i:s');
    $checkout_model->insertInOrder($uid, $order_date);
    foreach ($_SESSION['cart_items'] as $key => $value){
       $isDone = $checkout_model->insertInOrderItems($checkout_model->selectOrderId($uid, $order_date), $key);
    }
    if ($isDone){
        //Verwijder $_SESSION[cart-items].
        //Redirect naar Owned courses.
        unset($_SESSION['cart_items']);
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
        header("Location: $actual_link/wfflix/thank-you");
    }
    else{
        //Meld de gebruiker dat er iets verkeerd is gegaan en vertel contact op te nemen met wfflix via contact form met als subject order gecannceld.
        echo"Something has gone wrong please contact us on our contact form. With subject: order cancelled.";
    }
}


require 'views/navigation.php';
require 'views/checkout.view.php';