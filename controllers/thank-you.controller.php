<?php
session_start();
$pageTitle = "Thank you";
global $keys;

$keys = [];

if (!empty($_SESSION['cart_items'])) {
    foreach
    ($_SESSION['cart_items'] as $key => $value) {
        array_push($keys, $key);
    }
}
if (!empty($_SESSION['userName'])){
    $user_name = $_SESSION['userName'];
}
else{
    $user_name = "Sign in";
}
if (isset($_GET['subject'])){
    $_SESSION['subject'] = $_GET['subject'];
    session_commit();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results");
}

if(isset($_POST['goToOwnedCourses'])) {
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/owned-courses");
}

if(isset($_POST['goToNewCourse'])) {
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/new-courses");
}
require 'views/navigation.php';
require 'views/thank-you.view.php';