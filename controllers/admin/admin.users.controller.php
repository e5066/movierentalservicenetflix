<?php

include 'views/admin/admin.navigation.php';
require 'models/Get_users_model.php';

global $numbers;
$users = new Get_users_model();
$numbers = $users->getUsers();

include 'views/admin/admin.users.view.php';