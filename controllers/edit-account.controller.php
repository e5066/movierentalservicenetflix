<?php
session_start();
global $user_name, $firstname, $lastname, $email, $username;
$pageTitle = "Edit account";
if (!empty($_SESSION['userName'])){
    $user_name = $_SESSION['userName'];
}
else{
    $user_name = "Sign in";
}
if (isset($_GET['subject'])){
    $_SESSION['subject'] = $_GET['subject'];
    session_commit();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results");
}
require 'models/edit_account_model.php';
require 'views/navigation.php';

$regex_pattern = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,32}$/";

$ed_acc_model = new edit_account_model();

global $changed_pw_err, $pw_err, $edit_err, $success_msg, $succ_edit;

if (!empty($_SESSION['userName'])){
    $user_name = $_SESSION['userName'];
}
else {
    $user_name = "Sign in";
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/login");


}


if(isset($_POST['edit'])){
    $succ_edit = "";
    $edit_err = "";
    $changed_profile = $ed_acc_model->editProfile($_POST['firstname'], $_SESSION['userId'], $_POST['lastname'], $_POST['email'], $_POST['username']);
    if ($changed_profile == false) {
        $edit_err = "Something went wrong";
    } else {

    }

}

if (isset($_POST['changepw'])) {
    $pw_err = "";
    $changed_pw_err = "";
    $success_msg = "";
    if ( $_POST['pw1'] ==  $_POST['pw2']) {
        if (!preg_match($regex_pattern, $_POST['pw1'])) {
            $color = "red";
            $pw_err = "Minimum eight and maximum 32 characters, at least one uppercase letter,<br>one lowercase letter, one number and one special character.";
        } else {
            $changed_pw = $ed_acc_model->changePassword($_SESSION['userId'], $_POST['pw1'],
                password_hash($_POST['pw1'], PASSWORD_DEFAULT));
            if ($changed_pw == true) {
                $success_msg = "Your password has been changed";
            }
            else {
                $changed_pw_err = "Something went wrong, try again";
            }
        }
    }
    else {
        $changed_pw_err = "Passwords do not match";
    }
}



$user = $ed_acc_model->getProfileInfo($_SESSION['userId']);
$firstname = $user[0];
$lastname = $user[1];
$email = $user[2];
$username = $user[3];
require 'views/edit-accout.view.php';
include('views/footer.php');




