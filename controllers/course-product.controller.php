<?php
if ( !isset($_SESSION) ) session_start();
$pageTitle = "Course product - " . $_GET['id'];
if (!empty($_SESSION['userName'])){
    $user_name = $_SESSION['userName'];
}
else{
    $user_name = "Sign in";
}

global $setValue, $hasCourse;

if (!empty($_SESSION['userName'])){
    $user_name = $_SESSION['userName'];
    $setValue = "Add to cart";
}
else{
    $user_name = "Sign in";
    $setValue = "Buy";
}
if (isset($_GET['subject'])){
    $_SESSION['subject'] = $_GET['subject'];
    session_commit();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results");
}
require 'views/navigation.php';
if (empty($_SESSION['cart_items'])){
    $_SESSION['cart_items'] = [];
}
else{
    //$_SESSION['cart_items'] = null;
}

//include 'views/course-product.view.php';
require 'models/CourseProductModel.php';
require 'models/Cart_model.php';

global $collectedCourses1;
$courses = new CourseProductModel();
$cartModel = new Cart_model();

//$id = $_GET['id'] ? intval($_GET['id']) : 0;

if(isset($_GET['id'])) {
    $collectedCourses1 = $courses->getCourses($_GET['id']);

    if (!array_key_exists($_GET['id'], $_SESSION['cart_items'])) {
        $_SESSION['cart_items'][$_GET['id']] = $cartModel->test($_GET['id']);
    }
    else {
    }
}
if (isset($_SESSION['userId']) && $courses->hasUserCourse($_SESSION['userId'], $_GET['id'])){
    $hasCourse = true;
}
else{
    $hasCourse = false;
}
require 'views/course-product.view.php';

