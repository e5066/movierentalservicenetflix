<?php
if ( !isset($_SESSION) ) session_start();
$pageTitle = "Forgot password";
if (!empty($_SESSION['userName'])){
    $user_name = $_SESSION['userName'];
}
else{
    $user_name = "Sign in";
}
if (isset($_GET['subject'])){
    $_SESSION['subject'] = $_GET['subject'];
    session_commit();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results");
}
require 'views/navigation.php';
require 'core/Php_mail_sender.php';
require 'models/Reset_password_model.php';

global $mail_msg, $color;

$reset_class = new Reset_password_model();

$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

if (isset($_POST['resetBtn']) && !empty($_POST['emailInput'])){
    $characters = '0123456789@!$abcdefghijklmnopqrstuvwxyz@!$ABCDEFGHIJKLMNOPQRSTUVWXYZ@!$';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 14; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    $fullname = $reset_class->getUserName($_POST['emailInput']);
    $message = "<h1>Dear $fullname,</h1>
                <h3>New password: <I>$randomString</I></h3>" . "<br>" .
                "<a href='$actual_link/login'>Click here to login with new password</a>" . "<br>" .
                "<br><I>With kind regards,</I> <br>" .
                "<br><I>WFFLIX - Team B2</I>";
    if (empty($fullname)){
        $color = "red";
        $mail_msg = "Could not find account.";
    }
    else{
        Php_mail_sender::forgotPassword($_POST['emailInput'], $fullname, $message);
        if (!empty(Php_mail_sender::$failed)){
            $color = "red";
            $mail_msg = Php_mail_sender::$failed;
        }
        elseif(!empty(Php_mail_sender::$success)){
            if ($reset_class->resetPwOfUser($_POST['emailInput'], password_hash($randomString, PASSWORD_DEFAULT))){
                $color = "green";
                $mail_msg = Php_mail_sender::$success;
                header("Refresh:3");
            }
            else{
                $color = "red";
                $mail_msg = "Could not reset password,<br> contact us using the contact form!";
            }
        }
        else{
            $color = "red";
            $mail_msg = "Something went wrong";
        }
    }
}
else{
    if(isset($_POST['resetBtn'])){
        $color = "red";
        $mail_msg = "Email is empty!";
    }
}

require 'views/forgot-password.view.php';
