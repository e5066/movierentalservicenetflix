<?php
if ( !isset($_SESSION) ) session_start();

$pageTitle = "Start course";
if (!empty($_SESSION['userName'])){
    $user_name = $_SESSION['userName'];
}
else{
    $user_name = "Sign in";
}
if (isset($_GET['subject'])){
    $_SESSION['subject'] = $_GET['subject'];
    session_commit();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results");
}
require 'views/navigation.php';
global $course_url, $course_title, $course_description, $course_items, $channelTitle, $counter, $channel_url, $rating, $language, $nextVideo;

$apiKey = "AIzaSyDeKkurGNQ6PEYTx_CH922eORO_nhglK7k";

$course_items = [];
$counter = 0;

require_once 'core/GoogleApi/vendor/autoload.php';
require_once 'models/Start_course_model.php';

$start_course_model = new Start_course_model();

$client = new Google_Client();
$client->setDeveloperKey($apiKey);
$youtube = new Google_Service_YouTube($client);

$nextPageToken = '';
$course_url = "https://www.youtube.com/embed/";


if (isset($_GET['video'])){
    $start_course_model->saveCurrentSubject($_SESSION['userId'], $_GET['id'], $_GET['video']);
    $queryParams = [
        'id' => $_GET['video']
    ];
    $response = $youtube->videos->listVideos('snippet,contentDetails', $queryParams);
    $course_title = $response['items'][0]['snippet']['title'];
    $course_description = wordwrap($response['items'][0]['snippet']['description'], 400, "<br><br>");
    do {
        $playlistItemsResponse = $youtube->playlistItems->listPlaylistItems('snippet, contentDetails', array(
            'playlistId' => $start_course_model->getPlaylistId($_GET['id'])[0],
            'maxResults' => 50,
            'pageToken' => $nextPageToken));

        foreach ($playlistItemsResponse['items'] as $playlistItem) {
            $counter += 1;
            array_push($course_items, array($playlistItem['snippet']['title'], $playlistItem['snippet']['description'],$playlistItem['snippet']['resourceId']['videoId'], $playlistItem['snippet']['thumbnails']['high']['url'], $playlistItem['snippet']['channelTitle'], $playlistItem['snippet']['channelId']));
        }
        $nextPageToken = $playlistItemsResponse['nextPageToken'];
    }while ($nextPageToken <> '');
    $channelTitle = $course_items[0][4];
    $channel_url = $course_items[0][5];
    $rating = $start_course_model->getPlaylistId($_GET['id'])[1];
    $language = $start_course_model->getPlaylistId($_GET['id'])[2];
    $nextVideoKey = array_search($_GET['video'], array_column($course_items, 2));
    $nextVideoKey += 1;
    if (array_key_exists($nextVideoKey, $course_items)){
        $nextVideo = $course_items[$nextVideoKey];
    }
    else{
        $nextVideo = null;
    }
}
elseif ($start_course_model->checkCourseChapter($_SESSION['userId'], $_GET['id']) != "empty"){
    $_GET['video'] = $start_course_model->checkCourseChapter($_SESSION['userId'], $_GET['id']);
    $queryParams = [
        'id' => $_GET['video']
    ];
    $response = $youtube->videos->listVideos('snippet,contentDetails', $queryParams);
    $course_title = $response['items'][0]['snippet']['title'];
    $course_description = wordwrap($response['items'][0]['snippet']['description'], 400, "<br><br>");
    do {
        $playlistItemsResponse = $youtube->playlistItems->listPlaylistItems('snippet, contentDetails', array(
            'playlistId' => $start_course_model->getPlaylistId($_GET['id'])[0],
            'maxResults' => 50,
            'pageToken' => $nextPageToken));

        foreach ($playlistItemsResponse['items'] as $playlistItem) {
            $counter += 1;
            array_push($course_items, array($playlistItem['snippet']['title'], $playlistItem['snippet']['description'],$playlistItem['snippet']['resourceId']['videoId'], $playlistItem['snippet']['thumbnails']['high']['url'], $playlistItem['snippet']['channelTitle'], $playlistItem['snippet']['channelId']));
        }
        $nextPageToken = $playlistItemsResponse['nextPageToken'];
    }while ($nextPageToken <> '');
    $channelTitle = $course_items[0][4];
    $channel_url = $course_items[0][5];
    $rating = $start_course_model->getPlaylistId($_GET['id'])[1];
    $language = $start_course_model->getPlaylistId($_GET['id'])[2];
    $nextVideoKey = array_search($_GET['video'], array_column($course_items, 2));
    $nextVideoKey += 1;
    if (array_key_exists($nextVideoKey, $course_items)){
        $nextVideo = $course_items[$nextVideoKey];
    }
    else{
        $nextVideo = null;
    }
}
else{
    do {
        $playlistItemsResponse = $youtube->playlistItems->listPlaylistItems('snippet, contentDetails', array(
            'playlistId' => $start_course_model->getPlaylistId($_GET['id'])[0],
            'maxResults' => 50,
            'pageToken' => $nextPageToken));

        foreach ($playlistItemsResponse['items'] as $playlistItem) {
            $counter += 1;
            array_push($course_items, array($playlistItem['snippet']['title'], $playlistItem['snippet']['description'],$playlistItem['snippet']['resourceId']['videoId'], $playlistItem['snippet']['thumbnails']['high']['url'], $playlistItem['snippet']['channelTitle'], $playlistItem['snippet']['channelId']));
        }
        $nextPageToken = $playlistItemsResponse['nextPageToken'];
    }while ($nextPageToken <> '');

    $course_title = $course_items[0][0];
    $course_description = wordwrap($course_items[0][1], 400, "<br><br>");
    $channelTitle = $course_items[0][4];
    $channel_url = $course_items[0][5];
    $rating = $start_course_model->getPlaylistId($_GET['id'])[1];
    $language = $start_course_model->getPlaylistId($_GET['id'])[2];
    $_GET['video'] = $course_items[0][2];
    $nextVideoKey = array_search($_GET['video'], array_column($course_items, 2));
    $nextVideoKey += 1;
    if (array_key_exists($nextVideoKey, $course_items)){
        $nextVideo = $course_items[$nextVideoKey];
    }
    else{
        $nextVideo = null;
    }
}
require 'views/start-course.view.php';
