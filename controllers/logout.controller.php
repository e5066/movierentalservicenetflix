<?php
if ( !isset($_SESSION) ) session_start();
$pageTitle = "Logout";
global $user_name;
(!empty($_SESSION['userName']) ? $user_name = $_SESSION['userName'] : $user_name = "Sign in");
if (isset($_GET['subject'])){
    $_SESSION['subject'] = $_GET['subject'];
    session_commit();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results");
}
require 'views/navigation.php';
require 'views/logout.view.php';

if (!empty($_SESSION['userName'])) {
    session_destroy();
}
?>
<?php ?>
    <meta http-equiv="Refresh" content="0; url=/wfflix/login">
<?php ?>