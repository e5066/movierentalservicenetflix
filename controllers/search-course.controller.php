<?php
if ( !isset($_SESSION) ) session_start();
$pageTitle = "Search course";
global $collectedCourses;
if (!empty($_SESSION['userName'])){
    $user_name = $_SESSION['userName'];
}
else{
    $user_name = "Sign in";
}
require 'models/Search_courses_model.php';

global $collectedCourses;
$courses = new Search_courses_model();

$collectedCourses = $courses->getCourses($_SESSION['subject']);
if (isset($_GET['subject'])){
    $_SESSION['subject'] = $_GET['subject'];
    session_commit();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results");
}
require 'views/navigation.php';
require 'views/search-results.view.php';
