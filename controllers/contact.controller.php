<?php
if ( !isset($_SESSION) ) session_start();
$pageTitle = "Contact";
if (!empty($_SESSION['userName'])){
    $user_name = $_SESSION['userName'];
}
else{
    $user_name = "Sign in";
}
if (isset($_GET['subject'])){
    $_SESSION['subject'] = $_GET['subject'];
    session_commit();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results");
}
require 'views/navigation.php';
global $user_name;
require 'core/Php_mail_sender.php';

global $success, $failed;

$success = "";
$failed = "";

if (!empty($_POST['firstName']) && !empty($_POST['lastName']) && !empty($_POST['mail']) && !empty($_POST['message']) && !empty($_POST['subject'])){

$firstName=$_POST['firstName'];
$lastName=$_POST['lastName'];
$mail=$_POST['mail'];
$subject=$_POST['subject'];
$message=$_POST['message'];


$message = "
<h1 style='visibility: hidden'>
Subject: $subject</h1>
<h4>$message</h4>
<br>
<I>With kind regards,
<br>
$firstName $lastName
<br>
$mail <I>
<br>
<img src='https://www.pngarts.com/files/12/Call-Centre-Agent-PNG-Free-Download.png'>
<p style='visibility: hidden'></p>
";

?>
<?php
Php_mail_sender::sendQuestionMail($_POST['firstName'],$_POST['lastName'], $_POST['mail'],$_POST['subject'], $message);
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    $success = Php_mail_sender::$success;
    $failed = Php_mail_sender::$failed;
    ?>
    <?php ?>
    <meta http-equiv="Refresh" content="5; url=/wfflix/contact">
    <?php ?>
<?php
$failed="";
}
elseif(isset($_POST['submit']) == "Verzenden" ){
    $failed = "Not all required fields are filled in!";
}

require 'views/contact.view.php';