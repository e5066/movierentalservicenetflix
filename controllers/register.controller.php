<?php
if ( !isset($_SESSION) ) session_start();
$pageTitle = "Register";
global $error_msg, $color;

//PASSWORD REGEX: Minimum eight and maximum 32 characters, at least one uppercase letter, one lowercase letter, one number and one special character:
$regex_pattern = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,32}$/";
if (!empty($_SESSION['userName'])){
    $user_name = $_SESSION['userName'];
}
else{
    $user_name = "Sign in";
}
if (isset($_GET['subject'])){
    $_SESSION['subject'] = $_GET['subject'];
    session_commit();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results");
}
require 'views/navigation.php';
require 'models/Login_Register_model.php';

$lr_model = new Login_Register_model();

if($_SERVER["REQUEST_METHOD"] == "POST"){
    if (empty($_POST['nameInput']) || empty($_POST['lastnameInput']) || empty($_POST['emailInput']) || empty($_POST['passwordInput']) || empty($_POST['confirmPasswordInput'])){
        $color = "red";
        $error_msg = "Required fields are empty.";
    }
    else{
        if (!filter_var($_POST['emailInput'], FILTER_VALIDATE_EMAIL)){
            $color = "red";
            $error_msg = "Invalid mail.";
        }
        if ($lr_model->validateIfUserExists($_POST['emailInput']) != 0){
            switch ($lr_model->validateIfUserExists($_POST['emailInput'])){
                case 1:
                    $color = "red";
                    $error_msg = "Account already exists.";
                    break;
                case 2:
                    $color = "red";
                    $error_msg = "Oops! Something went wrong. Please try again later.";
            }
        }
        else{
            if (!preg_match($regex_pattern, $_POST['passwordInput'])){
                $color = "red";
                $error_msg = "Minimum eight and maximum 32 characters, at least one uppercase letter,<br>one lowercase letter, one number and one special character.";
            }
            else{
                if ($_POST['confirmPasswordInput'] != $_POST['passwordInput']){
                    $color = "red";
                    $error_msg = "Password did not match.";
                }
                else{
                    if ($lr_model->registerAccount($_POST['nameInput'], $_POST['lastnameInput'], $_POST['emailInput'], password_hash($_POST['passwordInput'], PASSWORD_DEFAULT)) == true){
                        $color = "green";
                        $error_msg = "You're now a member! You will be send to the login page";
                        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
                        ?>
                        <?php ?>
                        <meta http-equiv="refresh" content="5;url=<?=$actual_link?>/wfflix/login">
                        <?php ?>
                        <?php
                    }
                }
            }
        }
    }
}


require 'views/register.view.php';
?>