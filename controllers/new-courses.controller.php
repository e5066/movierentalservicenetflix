<?php
if ( !isset($_SESSION) ) session_start();
$pageTitle = "Shop";
if (!empty($_SESSION['userName'])){
    $user_name = $_SESSION['userName'];
}
else{
    $user_name = "Sign in";
}
if (isset($_GET['subject'])){
    $_SESSION['subject'] = $_GET['subject'];
    session_commit();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results");
}
require 'views/navigation.php';
require 'models/Filter_courses_model.php';
$filter = new Filter_courses_model();
global $subjects;
global $foundSubjects;
global $selectedSubject, $selectedScore, $selectedLevel;
global $most_popular, $highly_rated, $newest;
global $top_picks;

//Get all subjects and return those subjects to a global var so that we have a dynamic select options.
$subjects = $filter->getAllSubjects();

$top_picks = $filter->orderBy($filter->generateOrderByQuery("top10", "", ""));

/**
 * Checks if filter items have been selected, if so than add those values to a global @var
 * the global variables are needed to fill the generateQuery method located in Filter_courses_model.
 */
if (isset($_POST['filterSubject'])){
    $selectedSubject = $_POST['filterSubject'];
}
if (isset($_POST['scores'])){
    $selectedScore = $_POST['scores'];
}
if (isset($_POST['levels'])){
    $selectedLevel = $_POST['levels'];
}

if (isset($_POST['orderBy']) && $_POST['orderBy'] === "most-popular"){
    $most_popular = $_POST['orderBy'];
}
if (isset($_POST['orderBy']) && $_POST['orderBy'] === "highly-rated"){
    $highly_rated = $_POST['orderBy'];
}
if (isset($_POST['orderBy']) && $_POST['orderBy'] === "newest"){
    $newest = $_POST['orderBy'];
}

//This is for the search-bar in the navbar and it will check if user enters something into it.
//If so it will send the user to the search-results page.
if(isset($_GET['search-on-course'])) {
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results?search-on-course=".$_GET['search-on-course']);
    exit();
}
/**
This part is for the filter system.
We have 3 options:
- select your rating so 1, 2, 3, 4 or 5 stars.
- select subject so this means "PHP, Databases, java" etc.
- select level, every programming language or subject in the IT-Business has multiple levels of how hard it is to learn. In this case we have the next options:
    - Beginner.
    - Intermediate.
    - Advanced.
    - Expert.
*/
elseif (isset($_POST['submitFilter'])){
    $foundSubjects = $filter->getFilteredCourses($filter->generateFilteredQuery($selectedSubject, $selectedScore, $selectedLevel));
    require 'views/buy-courses.view.php';
}
elseif (isset($_POST['orderBy'])){
    $foundSubjects = $filter->orderBy($filter->generateOrderByQuery($most_popular, $highly_rated, $newest));
    require 'views/buy-courses.view.php';
}
else {
    $foundSubjects = $filter->fillShop();
    require 'views/buy-courses.view.php';
}








