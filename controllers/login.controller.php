<?php
if ( !isset($_SESSION) ) session_start();
$pageTitle = "Login";
$user_name = "Sign in";

if (isset($_GET['subject'])){
    $_SESSION['subject'] = $_GET['subject'];
    session_commit();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results");
}
require 'views/navigation.php';
require 'models/Login_Register_model.php';
global $mail_err, $login_err, $user, $status, $disabled;
$lr_model = new Login_Register_model();

if(isset($_COOKIE['auth_timer']) && $_COOKIE['auth_timer'] == true){
    $status = "disabled";
    $disabled = "disabled";
}
else{
    $status = "";
    $disabled = "";
}
(!isset($_SESSION['loginCount']) ? $_SESSION['loginCount'] = 0 : "");
if (isset($_POST['loginBtn']) && !empty($_POST['emailInput'])){
    if (!filter_var($_POST['emailInput'], FILTER_VALIDATE_EMAIL)){
        $mail_err = "Invalid mail";
    }
    else
    {
        if ($_POST['passwordInput']){
            if (!empty($lr_model->validateUser($_POST['emailInput']))){
                $hashed_pw = $lr_model->validateUser($_POST['emailInput']);
                if (password_verify($_POST['passwordInput'], $hashed_pw)){
                    if(!empty($lr_model->login($_POST['emailInput'], $hashed_pw))) {
                        $user = $lr_model->login($_POST['emailInput'], $hashed_pw);
                        $_SESSION['userId'] = $user[0];
                        $_SESSION['userName'] = $user[1];
                    }
                    else {
                        $login_err = "Could not find account. Contact us, using the contact form.";
                    }
                }
                else{
                    if ($_SESSION['loginCount'] < 3){
                        switch ($_SESSION['loginCount']){
                            case 0:
                                $login_err = "Wrong password, 3 attempts left";
                                break;
                            case 1:
                                $login_err = "Wrong password, 2 attempts left";
                                break;
                            case 2:
                                $login_err = "Wrong password, 1 attempt left";
                                break;
                        }
                        $_SESSION['loginCount'] += 1;
                    }
                    else{
                        $login_err = "To many login attempts, try again after 2 hours.";
                        setcookie('auth_timer', true, time()+60*120);
                        header("Refresh:3");
                    }
                }
            }
            else {
                $login_err = "Could not find account, with current mail.";
            }
        }
        else{
            $login_err = "Password not set";
        }
    }
}
else{
    (isset($_POST['loginBtn']) ? $login_err = "Required field's' are empty." : null);
}

//(empty($_POST['emailInput']) && empty($_POST['passwordInput']) && isset($_POST['loginBtn']) ? $login_err = "Required field's' empty." : $login_err = "");

if (!empty($_SESSION['userId']) && !empty($_SESSION['userName'])){
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/edit-account");
    exit();
}
else{
    require 'views/login.view.php';
}