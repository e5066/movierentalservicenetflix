<?php
if ( !isset($_SESSION) ) session_start();
$pageTitle = "Owned courses";
global $boughtCourses;
if (!empty($_SESSION['userName'])){
    $user_name = $_SESSION['userName'];
}
else{
    $user_name = "Sign in";
}
if (isset($_GET['subject'])){
    $_SESSION['subject'] = $_GET['subject'];
    session_commit();
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/search-results");
}
require 'views/navigation.php';
require 'models/Owned_courses_model.php';
$owned_courses_model = new Owned_courses_model();

if(!empty($_SESSION['userId'])) {
    if (!empty($owned_courses_model->getBoughtCourses($_SESSION['userId']))){
        $boughtCourses = $owned_courses_model->getBoughtCourses($_SESSION['userId']);
    }
    else{
        $boughtCourses = null;
    }
}
else{
    $boughtCourses = null;
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/wfflix/login");
}





require 'views/owned-courses.view.php';
include('views/footer.php');