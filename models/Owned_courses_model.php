<?php

require_once 'core/db_connection.php';

class Owned_courses_model extends db_connection
{
    public function getBoughtCourses($user_id){
        try {
            $allCollectedCourses = array();
            $stmt = $this->connect()->prepare("SELECT courses.id, courses.name, courses.description, courses.chapter, courses.author, courses.picture FROM courses INNER JOIN order_items on order_items.cid = courses.id INNER JOIN orders on orders.id = order_items.oid INNER JOIN users on users.id = orders.uid WHERE users.id = :userId;");
            $stmt->execute([':userId' => $user_id]);
            while ($row = $stmt->fetch()) {
                array_push($allCollectedCourses, array($row['id'], $row['name'], $row['description'], $row['chapter'], $row['author'], $row['picture']));
            }
            return $allCollectedCourses;
        } catch (PDOException $ex) {
            return $ex;
        }
    }

}