<?php

require_once 'core/db_connection.php';


class CourseProductModel {

    public function getCourses($id) : array
    {
        $courses = array();
        $dbconn = new db_connection();
        $stmt = $dbconn->connect()->query("SELECT * FROM `courses` WHERE id = '$id'");
        while ($row = $stmt->fetch()) {
            array_push($courses, array($row['id'],$row['name'], $row['description'], $row['chapter'], $row['language'], $row['price'], $row['picture'], $row['rating'], $row['level'], $row['date_added']));
        }
        return $courses;
    }

    //checks if user already has course
    public function hasUserCourse($uid, $cid){
        try
        {
            $isTrue=false;
            $dbconn = new db_connection();
            $stmt = $dbconn->connect()->prepare("SELECT * FROM order_items INNER JOIN orders on orders.id = order_items.oid WHERE uid = :uid AND cid = :cid;");
            $stmt->execute([':uid' => $uid, ':cid' => $cid]);

            while ($row = $stmt->fetch()) {
                if ($stmt->rowCount() >= 1){
                    $isTrue = true;
                }
                else{
                    $isTrue = false;
                }
            }
            if ($isTrue == true){
                return true;
            }
            else{
                return false;
            }
        }
        catch (PDOException $ex)
        {
            return false;
        }
    }
}