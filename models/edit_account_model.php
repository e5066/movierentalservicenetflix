<?php

require_once 'core/db_connection.php';


class edit_account_model extends db_connection
{
    private array $user;

    public function __construct()
    {
        $this->user = array();
    }
    public function editProfile($firstname, $id, $lastname, $email, $username)
    {
//        echo $id;
        // Prepare an insert statement
        $sql = ("UPDATE users SET name = :firstname, lastname = :lastname, email = :email, username = :username WHERE id = :id");



        if ($stmt = $this->connect()->prepare($sql)) {
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":firstname", $firstname, PDO::PARAM_STR);
            $stmt->bindParam(":lastname", $lastname, PDO::PARAM_STR);
            $stmt->bindParam(":email", $email, PDO::PARAM_STR);
            $stmt->bindParam(":username", $username, PDO::PARAM_STR);
            $stmt->bindParam(":id", $id, PDO::PARAM_STR);

            // Attempt to execute the prepared statement
            if ($stmt->execute()) {
                return true;
            }
        }
        return false;
    }

    public function changePassword($id, $pw1, $hashedpw) {
            $sql = ("UPDATE users SET password = :hashedpw WHERE id = :id");

            if ($stmt = $this->connect()->prepare($sql)) {
                // Bind variables to the prepared statement as parameters
                $stmt->bindParam(":hashedpw", $hashedpw, PDO::PARAM_STR);
                $stmt->bindParam(":id", $id, PDO::PARAM_STR);

                // Attempt to execute the prepared statement
                if ($stmt->execute()) {
                    return true;
                }
        }
        return false;
    }
    public function getProfileInfo($id)
    {
        $sql = "SELECT name, lastname, email, username FROM users WHERE id = :id";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(['id'=> $id]);

        while ($row = $stmt->fetch()) {
            array_push($this->user, $row['name'], $row['lastname'], $row['email'], $row['username']);
        }
        return $this->user;
    }


}

