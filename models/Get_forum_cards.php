<?php
require_once 'core/db_connection.php';

class Get_forum_cards
{
    public function getCards() {
        $cards = array();
        $dbconn = new db_connection();
        $stmt = $dbconn->connect()->query("SELECT cards.*, users.username, tags.tag FROM cards 
                                                    JOIN users ON cards.userId = users.id 
                                                    JOIN tags ON cards.tags = tags.tags_id;");
        while ($row = $stmt->fetch()) {
            array_push($cards, array(
                $row['cardId'], $row['title'], $row['description'], $row['votes'],
                $row['answers'], $row['views'], $row['tags'], $row['userId'], $row['username'], $row['tag']
            ));
        }
        return $cards;
    }

}