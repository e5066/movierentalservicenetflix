<?php

require_once 'core/db_connection.php';

class Start_course_model extends db_connection
{
    public function getPlaylistId($id){
        try {
            $allCollectedCourses = array();
            $stmt = $this->connect()->prepare("SELECT yt_url, rating, languages.language FROM courses INNER JOIN languages ON languages.id = courses.language WHERE courses.id = :id;");
            $stmt->execute([':id' => $id]);
            while ($row = $stmt->fetch()) {
                array_push($allCollectedCourses, $row['yt_url'], $row['rating'], $row['language']);
            }
            return $allCollectedCourses;
        } catch (PDOException $ex) {
            return $ex;
        }
    }

    public function checkCourseChapter($uid, $cid){
        try
        {
            $stmt = $this->connect()->prepare("SELECT chapter FROM user_has_active_courses WHERE uid = :uid AND cid = :cid;");
            $stmt->execute([':uid' => $uid, ':cid' => $cid]);
            while ($row = $stmt->fetch()) {
                if ($stmt->rowCount() >= 1){
                    $chapter = $row['chapter'];
                }
            }
            if (!empty($chapter)){
                return $chapter;
            }
            else{
                return "empty";
            }
        }
        catch (PDOException $ex)
        {
            return "empty";
        }
    }

    public function saveCurrentSubject($uid, $cid, $videoId){
        try
        {
            if ($this->checkCourseChapter($uid, $cid) == "empty"){
                $stmt = $this->connect()->prepare("INSERT INTO user_has_active_courses (uid, cid, chapter) VALUES (:uid, :cid, :chapter);");
                if ($stmt->execute([':uid' => $uid, ':cid' => $cid, 'chapter' => $videoId])){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                $stmt = $this->connect()->prepare("UPDATE user_has_active_courses SET chapter = :chapter WHERE uid = :uid AND cid = :cid;");
                if ($stmt->execute([':uid' => $uid, ':cid' => $cid, 'chapter' => $videoId])){
                    return true;
                }
                else{
                    return false;
                }
            }
        }
        catch (PDOException $ex)
        {
            return false;
        }
    }
}