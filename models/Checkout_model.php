<?php

require_once 'core/db_connection.php';


class Checkout_model extends db_connection
{

    public function test($course_id)
    {
        try {
            $allcourses = array();

            //for every course in basket start new query with all data from specific course $course_item bv course id1
            //add to new array $allcourses and per course add new array. So multiple dimension array
            $stmt = $this->connect()->prepare("SELECT * FROM courses WHERE courses.id=$course_id;");
            $stmt->execute();
            while ($row = $stmt->fetch()) {
                array_push($allcourses, $row['name'], $row['price'], $row['id']);
            }
            var_dump($allcourses);
            return $allcourses;
        } catch (PDOException $ex) {
            return $ex;
        }

}
    //insert userid and order_date in orders table in database.
    public function insertInOrder($uid, $order_date)
    {

        $sql = "INSERT INTO orders (uid, order_date) VALUES (:uid, :order_date);";

        if ($stmt = $this->connect()->prepare($sql)) {
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":uid", $uid, PDO::PARAM_STR);
            $stmt->bindParam(":order_date", $order_date, PDO::PARAM_STR);


            // Attempt to execute the prepared statement
            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        }

    }

    //search when $uid,$order_date matches with orders.uid and orders.orders_date is the same.
    public function selectOrderId($uid, $order_date)
    {
        $sql = "SELECT orders.id from orders where orders.uid = :uid AND orders.order_date = :order_date";

        if ($stmt = $this->connect()->prepare($sql)) {
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":uid", $uid, PDO::PARAM_STR);
            $stmt->bindParam(":order_date", $order_date, PDO::PARAM_STR);

            // Attempt to execute the prepared statement
            if ($stmt->execute()) {
                while ($row = $stmt->fetch()) {
                    return $row['id'];
                }

            } else {
                return -1;
            }
        }

    }


    //insert order in table order_items
    public function insertInOrderItems($oid, $cid)
    {

        $sql = "INSERT INTO order_items (oid, cid) VALUES (:oid, :cid);";

        if ($stmt = $this->connect()->prepare($sql)) {
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":oid", $oid, PDO::PARAM_STR);
            $stmt->bindParam(":cid", $cid, PDO::PARAM_STR);


            // Attempt to execute the prepared statement
            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        }

    }
}