<?php

include_once './core/db_connection.php';

class Login_Register_model extends db_connection
{
    private array $user;

    public function __construct()
    {
        $this->user = array();
    }

    public function validateUser($mail): string
    {
        try {
            $stmt = $this->connect()->prepare("SELECT password FROM users WHERE email = :mail");
            $stmt->execute([':mail' => $mail]);
            $pw = "";
            while ($row = $stmt->fetch()) {
                $pw = $row['password'];
            }
            return $pw;
        } catch (PDOException $ex) {
            return $ex;
        }
    }

    public function login($mail, $pw)
    {
        try {
            $stmt = $this->connect()->prepare("SELECT id, name FROM users WHERE email = :mail AND password = :pw");
            $stmt->execute([':mail' => $mail, ':pw' => $pw]);
            while ($row = $stmt->fetch()) {
                array_push($this->user, $row['id'], $row['name']);
            }
            return $this->user;
        } catch (PDOException $ex) {
            return $ex;
        }
    }

    public function validateIfUserExists($mail)
    {
        $sql = "SELECT id FROM users WHERE email = :mail";
        if ($stmt = $this->connect()->prepare($sql)) {
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":mail", $mail, PDO::PARAM_STR);
            // Attempt to execute the prepared statement
            if ($stmt->execute()) {
                if ($stmt->rowCount() == 1) {
                    //means that account already exists.
                    return 1;
                } else {
                    //acount doesn't exists
                    return 0;
                }
            }
            else {
                return 2;
                //"Oops! Something went wrong. Please try again later.";
            }
        }
    }

    public function registerAccount($name, $lastname, $email, $password){
        // Prepare an insert statement
        $sql = "INSERT INTO users (name, lastname, email, password, username, role) VALUES (:name, :lastname, :email, :password, :username, 1)";

        if($stmt = $this->connect()->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":name", $name, PDO::PARAM_STR);
            $stmt->bindParam(":lastname", $lastname, PDO::PARAM_STR);
            $stmt->bindParam(":email", $email, PDO::PARAM_STR);
            $stmt->bindParam(":password", $password, PDO::PARAM_STR);
            $stmt->bindParam(":username", $name, PDO::PARAM_STR);

            // Attempt to execute the prepared statement
            if($stmt->execute()) {
                return true;
            }
            else{
                return false;
            }
        }
    }

}