<?php
require_once 'core/db_connection.php';

class Search_courses_model
{
    public function getCourses($search_pattern)
    {
        $courses = array();
        $dbconn = new db_connection();
        $stmt = $dbconn->connect()->query("SELECT * FROM `courses` WHERE name LIKE '%$search_pattern%'");
        while ($row = $stmt->fetch()) {
            array_push($courses, array($row['name'], $row['description'], $row['language'], $row['price'], $row['picture'], $row['id']));
        }
        return $courses;
    }
}