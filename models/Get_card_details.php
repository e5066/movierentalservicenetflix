<?php
require_once 'core/db_connection.php';

class Get_card_details
{
    public function getCardDetails() {
        $card_id = $_GET['var'];
        $cards_answers = array();
        $dbconn = new db_connection();
        $stmt = $dbconn->connect()->query(" SELECT cardId, message, users.username FROM forum 
                                                     JOIN users ON forum.userId = users.id                                         
                                                     WHERE forum.cardId IN (SELECT cards.cardId FROM cards WHERE cards.cardId = $card_id)");
        while ($row = $stmt->fetch()) {
            array_push($cards_answers, array(
                $row['cardId'], $row['message'], $row['username']
            ));
        }
//        echo var_dump($cards_answers);
        return $cards_answers;
    }

    public function getCardHeaders()
    {
        $card_id = $_GET['var'];
        $headers = array();
        $dbconn2 = new db_connection();
        $stmt2 = $dbconn2->connect()->query(" SELECT title, description, userId, users.username FROM cards 
                                                       INNER JOIN users ON users.id = cards.userId WHERE cardid = $card_id;");
        while ($row2 = $stmt2->fetch()) {
            array_push($headers, array(
                $row2['title'], $row2['description'], $row2['userId'], $row2['username']
            ));
        }
//        echo var_dump($headers);
        return $headers;
    }
}

