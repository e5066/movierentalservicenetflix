<?php
require_once 'core/db_connection.php';

/**
 * @author Yeray Guzmán Padrón.
 */

class Filter_courses_model
{
    /** @var bool $is_first Check if generateQuery attribute is first in query */
    private bool $is_first = true;

    //Collect all languages from the DB to have dynamic values inside our html form select option.
    public function getAllSubjects() : array{
        $subjects = array();
        $dbconn = new db_connection();
        $stmt = $dbconn->connect()->query("SELECT DISTINCT languages.language FROM languages INNER JOIN courses ON courses.language = languages.id;");
        while ($row = $stmt->fetch()) {
            array_push($subjects, array($row['language']));
        }
        return $subjects;
    }

    /**
     * Get query for filter from POST form
     *
     * @todo Validate $_POST to prevent SQL injections
     * @return string SQL query
     */
    public function generateFilteredQuery($subject, $scores, $level) : string
    {
        $q = "SELECT name, description, chapter, price, picture, rating, level, courses.id, author FROM courses INNER JOIN languages ON courses.language = languages.id";

        if ($subject !== "Select subject") {
            $q .= $this->checkWhereCondition() . "languages.language LIKE '%" . $subject . "%'";
        }

        if ($scores != 0) {
            $q .= $this->checkWhereCondition() . "rating='" . $scores . "'";
        }

        if ($level != 0) {
            $q .= $this->checkWhereCondition() . "courses.level='" . $level . "'";
        }
        $this->is_first = true;
        return $q . ";";
    }

    public function generateOrderByQuery($most_popular, $highly_rated, $newest) : string{
        if (!empty($most_popular)) {
            $q = "SELECT COUNT(cid) AS `most_popular`, name, description, chapter, language, price, picture, rating, author, level, courses.id, date_added  FROM wfflix.order_items INNER JOIN courses ON order_items.cid = courses.id GROUP BY cid ORDER BY `most_popular` DESC LIMIT 10;";
        }

        if (!empty($highly_rated)) {
            $q = "SELECT name, description, chapter, language, price, picture, rating, level, date_added, courses.id, author FROM courses ORDER BY rating DESC, price DESC LIMIT 10";
        }

        if (!empty($newest)) {
            $q = "SELECT name, description, chapter, language, price, picture, rating, level, date_added, courses.id, author FROM courses ORDER BY date_added DESC, price DESC LIMIT 10";
        }
        return $q . ";";
    }

    /**
     * Mutate $this->is_first if $_POST attribute is first or somewhere else in query.
     *
     * @return string " WHERE " || " AND "
     */
    private function checkWhereCondition() : string
    {
        if ($this->is_first) {
            $this->is_first = false;
            return " WHERE ";
        }
        return " AND ";
    }

    /**
     * The query attribute is filled in the controller with the value from the $this->generateQuery method.
     * If query is success then collect the array values and place them inside a multidimensional array.
     * The method has a return type of array that will be collected by the controller.
     */
    public function getFilteredCourses($query) : array{
        $subjects = array();
        $dbconn = new db_connection();
        $stmt = [];
        if (!empty($query)) {
            $stmt = $dbconn->connect()->query("$query");
        }
        else{
            array_push($subjects, 0, 0, 0, 0, 0, 0);
        }
        if ($stmt != null){
            while ($row = $stmt->fetch()) {
                array_push($subjects, array($row['name'], $row['description'], $row['chapter'], $row['price'], $row['picture'], $row['rating'], $row['level'], $row['id'], $row['author']));
            }
        }
        else{
            array_push($subjects, 0, 0, 0, 0, 0, 0);
        }
        return $subjects;
    }

    public function orderBy($query) : array{
        $selectedOrder = array();
        $dbconn = new db_connection();
        $stmt = [];

        if (!empty($query)) {
            $stmt = $dbconn->connect()->query("$query");
        }
        else{
            array_push($selectedOrder, 0, 0, 0, 0, 0, 0);
        }
        if ($stmt != null){
            while ($row = $stmt->fetch()) {
                array_push($selectedOrder, array($row['name'], $row['description'], $row['chapter'], $row['price'], $row['picture'], $row['rating'], $row['level'], $row['id'], $row['author']));
            }
        }
        else{
            array_push($selectedOrder, 0, 0, 0, 0, 0, 0);
        }
        return $selectedOrder;
    }

    public function fillShop(){
        $courses = array();
        $dbconn = new db_connection();
        $stmt = $dbconn->connect()->query("SELECT * FROM `courses` GROUP BY name ORDER BY name ASC LIMIT 20;");
        while ($row = $stmt->fetch()) {
            array_push($courses, array($row['name'], $row['description'], $row['chapter'], $row['price'], $row['picture'], $row['rating'], $row['level'], $row['id'], $row['author']));
        }
        return $courses;
    }
}