<?php

require_once 'core/db_connection.php';

class Cart_model extends db_connection
{
    public function test($course_id){
        try{
            $allcourses=array();

            //for every course in basket start new query with all data from specific course $course_item bv course id1
            //add to new array $allcourses and per course add new array. So multiple dimension array
                $stmt = $this->connect()->prepare("SELECT * FROM courses WHERE courses.id=$course_id;");
                $stmt->execute();
                while ($row = $stmt->fetch()) {
                    array_push($allcourses, $row['name'], $row['price'], $row['id']);
                }
            //var_dump($allcourses);
            return $allcourses;
        }
        catch(PDOException $ex){
            return $ex;
        }
    }
}


