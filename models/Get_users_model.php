<?php
require_once 'core/db_connection.php';

class Get_users_model
{
    public function getUsers() {
        $users = array();
        $dbconn = new db_connection();
        $stmt = $dbconn->connect()->query("SELECT * FROM `users`");
        while ($row = $stmt->fetch()) {
            array_push($users, array($row['id'], $row['username'], $row['name'], $row['lastname'], $row['email'], $row['role'], $row['user-id'], $row['institute'], $row['date']));
        }
        return $users;
    }
}